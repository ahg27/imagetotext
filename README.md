# Image to Text

The aim of this network is to take in photographs of text (e.g. letters) and to produce transcriptions.  The biggest problem in this is the reading of handwritten text, but there (might, in the future) also be considerations of RoI finding (not urgent currently, thanks to tightly cropped images of the text).

## Setup

This runs using Python3 in a virtual environment, setup using pipenv.

To train this network, use the [IAM Dataset](https://fki.tic.heia-fr.ch/databases/iam-handwriting-database) (you need to register to download it, but that's cool).  On second thought, might be able to use MNIST (NIST Special Database 19).  I'll survery what most people do when I have cracked the word segmentation, and therefore have some input for a net!

## Current state of play
Upgrading things from 'nothing works' to 'some things work'.  This is *progress*.  I'm breaking this into steps, rather than trying to just throw a whole image at a CNN and expecting it to work.  Most CNNs seem to process things a word at a time, so I'm following that lead.  In an ideal world, I'll have this workflow:

1. Detect the sheet
2. Remove headers and/or footers
3. Segment the image into lines
4. Segment the lines into words
5. Throw the words to the CNN and see how it fares
6. Correct the results, and use them to improve the net's training

Optional extras include incorporating a spell-check, and a nicer display, potentially web-based, showing image next to results to allow for easy correction.

### Sheet detection

Doesn't work.

I can apply edge detection to try and find the largest contour in order to pick out the sheet.  However, the chopping of the sheet by close cropping of the images means that I often chop off at that point, though some of the images suffer from even worse problems, where the lack of clear definition between background and sheet means that no large contour is found.  So I can apply a mask around the largest contour, but that is not going to work.

I've seen some blog posts on PyImageSearch that might be helpful (e.g. about detecting cards in an image) so will investigate that later.

### Line segmentation

I can currently use the guidelines on the sheet to find lines (utilising HoughLinesP), and have managed (with my test image) to get lines that are actually the real thing.  I get an extra line of the end of the sheet, but I'm not going to fight that fight just yet.

Long term - I would like to be able to not need guidelines to find my lines (some of my images don't have them, after all).  My first idea is to detect the change in colour / average intensity where there are no words to provide dark patches, but that is probably going to suffer heavily from background contamination, so that might need me to get sheet detection working.

### Word segmentation

This isn't working.  Not sure why.

### CNN

Ahahahaha.  I'll get back to you when I can produce some input to throw at it.  Getting there, I promise.