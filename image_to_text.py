import cv2
import numpy as np

"""
The main contenders for this work are TensorFlow, but also PyCaret sounds interesting and is very Python-y from
what I've seen
"""


def transcribe_image(image):
	return "letter_text"

	# Refer to Handwritten digit recognition with CNNs for tips on how to set up CNN (probably best for image recognition)
	# "Softmax is most likely activation you will want to use at the last layer of a classification task."
	# https://codelabs.developers.google.com/codelabs/tfjs-training-classfication/index.html


def update_training(image, text):
	# To Do:  Want to improve the net based on its work on each letter
	return None


def clean_image(img):
	# From https://towardsdatascience.com/faq-build-a-handwritten-text-recognition-system-using-tensorflow-27648fb18519
	# Might be best to do this after RoI has removed headers?

	# increase contrast - this is currently a problem because it breaks the dtype - maybe an explicit cast to uint8?
	# pxmin = np.min(img)
	# pxmax = np.max(img)
	# imgContrast = (img - pxmin) / (pxmax - pxmin) * 255

	# increase line width
	kernel = np.ones((3, 3), np.uint8)
	img_morph = cv2.erode(img, kernel, iterations=1)

	return img_morph