import cv2
import logging
import numpy as np

import image_preprocessing
import display_tools


def detect_sheet_by_contours(img):
    """
    A function to try to detect the area of the image occupied by the letter itself, and thereby remove the background.
    This is accomplished by:
        1. Blurring the image to remove artefacts in the background
        2. Applying edge detection
        3. Find continuous contours, and use the one with the largest area to define a bounding box
        4. Use the bounding box to apply a mask to the original image

    However, currently this is unreliable - subject particularly to getting confused by the edge of a letter dropping
    off the page, or certain backgrounds.
    """

    logging.warning("Sheet detection is currently very spotty in its results, and is not recommended if you're using a closely cropped image")

    img = display_tools.convert_image_to_greyscale(img)

    canny_output = image_preprocessing.apply_canny_edge_detection(img)

    # Close the image (dilate then erode) to try and get the letter boundary more continuous, using a simple kernel
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    closed_output = cv2.morphologyEx(canny_output, cv2.MORPH_CLOSE, kernel)

    # Try finding a continuous contour
    contours, hierarchy = cv2.findContours(closed_output,
                                           cv2.RETR_LIST,
                                           cv2.CHAIN_APPROX_SIMPLE)  # Don't approximate the contours until we know how what we've got to play with
    # Hierarchy is [next, previous, child, parent] for each contour - if no child (or parent) then -1
    # Don't care about parent/child - just going for largest contour regardless, so not bothering with
    # keeping the hierarchy at the moment
    # If you care about hierarchy, use cv2.RETR_TREE instead of cv2.RETR_LIST

    # Find the contour with the largest area, use this as the basis for a bounding rectangle for a letter mask
    max_contour_area, max_contour_id = 0, -1
    for i in range(len(contours)):
        if hierarchy[0][i][3] == -1:
            # Find the area of the contour in question
            contour_area = cv2.contourArea(contours[i])
            if contour_area > max_contour_area:
                max_contour_area = contour_area
                max_contour_id = i

    if logging.getLogger().getEffectiveLevel() <= logging.DEBUG:
        drawing_max_only = np.zeros((closed_output.shape[0], closed_output.shape[1], 3), dtype=np.uint8)
        cv2.drawContours(drawing_max_only, contours, max_contour_id, (255, 255, 255), 2, cv2.LINE_8, hierarchy, 0)
        cv2.imshow('Max Area Contour', display_tools.rescale_image_for_display(drawing_max_only))

    # Find the bounding rectangle around the largest contour, use it to produce a mask
    bounding_polygon = cv2.approxPolyDP(contours[max_contour_id], 3, True)
    bounding_rect = cv2.boundingRect(bounding_polygon)
    mask = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)
    cv2.rectangle(mask,
                  (int(bounding_rect[0]), int(bounding_rect[1])),
                  (int(bounding_rect[0] + bounding_rect[2]), int(bounding_rect[1] + bounding_rect[3])),
                  (255, 255, 255),
                  -1)

    if logging.getLogger().getEffectiveLevel() <= logging.DEBUG:
        cv2.rectangle(img, (int(bounding_rect[0]), int(bounding_rect[1])),
                      (int(bounding_rect[0] + bounding_rect[2]), int(bounding_rect[1] + bounding_rect[3])), (255, 0, 0),
                      3)
        cv2.imshow('Image with Bounding Box', display_tools.rescale_image_for_display(img))

    return cv2.bitwise_and(img, mask)


def detect_sheet_by_lines(img):
    img = display_tools.convert_image_to_greyscale(img)

    canny = image_preprocessing.apply_canny_edge_detection(img)

    lines = cv2.HoughLines(canny, 1, np.pi/100, 400)
    for line in lines:
        rho, theta = line[0]
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))
        cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), 2)

    return img


def remove_headers(img):
    """
    A function to remove the headers from the first pages of the letter

    Siamese networks could be useful here, apparently they don't need large training sets
    """
    if logging.getLogger().getEffectiveLevel() > logging.DEBUG:
        logging.info("Skipping remove_headers as it's not finished yet.  Set logging to DEBUG to run")
        return img

    logging.debug("Still working on getting this done")

    return img


"""
https://medium.com/srm-mic/color-segmentation-using-opencv-93efa7ac93e2
About colour segmentation - might be useful?
Trying to find a blue bird in a picture
import cv2 as cv
import matplotlib.pyplot as plt
from PIL import Image
!wget -nv https://static.independent.co.uk/s3fs-public/thumbnails/image/2018/04/10/19/pinyon-jay-bird.jpg -O bird.png
img = Image.open('./bird.png')
blur = cv.blur(img,(5,5))
blur0=cv.medianBlur(blur,5)
blur1= cv.GaussianBlur(blur0,(5,5),0)
blur2= cv.bilateralFilter(blur1,9,75,75)
hsv = cv.cvtColor(blur2, cv.COLOR_BGR2HSV)
low_blue = np.array([55, 0, 0])
high_blue = np.array([118, 255, 255])
mask = cv.inRange(hsv, low_blue, high_blue)
res = cv.bitwise_and(img,img, mask= mask)
"""
