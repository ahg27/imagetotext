import json
import logging
import os


def check_metadata(root_data_dir):
    """
    Just a simple function to do a quick sanity check on all the metadata I've created about my files
    1. Do all the files exist?
    2. Have I made any obvious mistakes with marking files as containing a header? (If there are headers, they should
       only be present on every 4th page)

    Potential checks for later:
    a. Are there any files present that do not have metadata? (Do I want to have to label all data files?)
    b. For a given group of letters, do they have consistent page numbering? (Is this really worth checking?  Is it
       information that is important?)
    """

    try:
        with open(os.path.join(root_data_dir, "metadata.json")) as metadata_file:

            files_correctly_located = 0
            files_missing = 0
            metadata = json.load(metadata_file)

            for item in metadata:
                for datafile in item["files"]:

                    # Check if the file exists
                    file_location = os.path.join(root_data_dir, item['directory'], datafile['name'])
                    if os.path.isfile(file_location):
                        files_correctly_located += 1
                    else:
                        logging.error(f"{file_location} is not found, despite being in metadata")
                        files_missing += 1

                    # Check if the file has been marked as having a header when it really shouldn't
                    if datafile['header'] and (datafile['page_number'] - 1) % 4:
                        logging.error(f"{file_location} is marked as having a header when it probably shouldn't be")

            logging.info(
                f"SUMMARY: {files_correctly_located} files found as expected, {files_missing} files are missing")

    except FileNotFoundError:
        logging.critical("No metadata file found!")
