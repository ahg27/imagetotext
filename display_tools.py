# Tools to scale images so that the screen can show them
# Based on the code at https://enumap.wordpress.com/2019/02/25/python-opencv-resize-image-fit-your-screen/

import cv2
import logging
import tkinter as tk

root = tk.Tk()

screensize = (root.winfo_screenwidth(), root.winfo_screenheight())


def rescale_image_for_display(original_img, extra_x_space=0, extra_y_space=120, force_enlargement=False):
	"""
	A function to shrink (or, if you really want, to enlarge) images so that they can be displayed on
	screen if necessary

	Also allows for further shrinking of the image if you don't want it to completely fill the screen
	Default for extra_y_space is non-zero to allow room for the menu bar
	"""
	screen_width, screen_height = screensize
	height = original_img.shape[0]
	width = original_img.shape[1]

	# Artificially shrink the screen size to give more space around the image after rescale
	screen_width -= extra_x_space
	screen_height -= extra_y_space

	scale_width = float(screen_width) / float(width)
	scale_height = float(screen_height) / float(height)
	if scale_height > scale_width:
		img_scale = scale_width
	else:
		img_scale = scale_height

	if img_scale >= 1 and not force_enlargement:
		return original_img

	logging.debug(f"Rescaling image from ({width},{height}) to screensize ({screen_width},{screen_height})")

	new_x, new_y = original_img.shape[1] * img_scale, original_img.shape[0] * img_scale
	new_img = cv2.resize(original_img, (int(new_x), int(new_y)))
	return new_img


def convert_image_to_greyscale(img):
	"""
	Most image processing seems to work best in greyscale.  Currently I load images in greyscale, so this isn't needed
	"""
	assert img.ndim in (2, 3)
	if img.ndim == 3:
		img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	return img
