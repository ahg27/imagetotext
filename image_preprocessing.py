import cv2
import logging

import display_tools


def apply_canny_edge_detection(img, debug_window_title="Edge image"):
    # Remove noise by applying a blurring
    # Mostly seems to remove artefacts in the background, but it can't hurt (can it?)
    img = cv2.GaussianBlur(img, (5, 5), 0)

    # Check if using bilateral filtering helps
    # It does not...
    # img_filtered = cv2.bilateralFilter(img, 7, 50, 50)

    # Apply edge detection
    canny_output = cv2.Canny(img, 30, 150)

    if logging.getLogger().getEffectiveLevel() <= logging.DEBUG:
        cv2.imshow(debug_window_title, display_tools.rescale_image_for_display(canny_output))

    return canny_output
