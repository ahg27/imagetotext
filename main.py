import argparse
import cv2
import logging
import os

# Local imports
import image_to_text
import roi_detection
import line_segmentation
import word_segmentation
import display_tools
import bookkeeping


def main(args):
    # Set debug as applicable
    logging_level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(level=logging_level)

    if args.checkmetadata:
        logging.info("Checking metadata of data input files for obvious errors")
        bookkeeping.check_metadata(args.checkmetadata)
        return

    if args.train:
        logging.critical("Training isn't created yet.")
        return

    extension_types = ('.jpg', '.jpeg')
    input_files = []
    for root, dirs, files in os.walk(args.input):
        for file in files:
            if file.lower().endswith(extension_types):
                input_files.append(os.path.join(root, file))

    for file_location in input_files:
        # Specifying to read the image in greyscale as everything seems to work best with that
        # I'm actually converting from BGR to grey, rather than reading in as greyscale
        # img = cv2.imread(file_location, cv2.IMREAD_GRAYSCALE)
        img = cv2.imread(file_location)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        if logging.getLogger().getEffectiveLevel() <= logging.DEBUG:
            cv2.imshow("Original image", display_tools.rescale_image_for_display(img))

        img = preprocess_image(img, detect_sheet=args.detectsheet, detect_header=args.detectheader)

        # Split the lines into individual words
        lines = line_segmentation.line_segmentation(img)

        if logging.getLogger().getEffectiveLevel() <= logging.DEBUG:
            for ii, line in enumerate(lines):
                cv2.imshow(f'Line {ii+1} of {len(lines)}', display_tools.rescale_image_for_display(line))

        words = []
        for line in lines[:1]:
            word_segmentation.prepare_img(line, 50)
            words.extend(word_segmentation.word_segmentation(line))

        logging.info(f"Found {len(words)} words")
        if logging.getLogger().getEffectiveLevel() <= logging.INFO:
            cv2.imshow(f'Line {1} of {len(lines)}', display_tools.rescale_image_for_display(lines[0]))
            for ii, word in enumerate(words):
                if ii > 10:
                    break
                (_, word_img) = word
                cv2.imshow(f'Word {ii+1} of {len(words)}', word_img)
                # cv2.imshow(f'Word {ii+1} of {len(words)}', display_tools.rescale_image_for_display(line))

        cv2.waitKey()

        # Read the letter
        # text = image_to_text.transcribe_image(img)


def preprocess_image(img, detect_sheet=False, detect_header=False):
    """
    Apply optional preprocessing before you get down to trying to detect lines and words
    1. Try to detect a sheet and mask over that
    2. Try  to detect if the sheet has a header that you can mask out
    """

    # Isolate the letter from background
    if detect_sheet:
        img = roi_detection.detect_sheet_by_lines(img)
        if logging.getLogger().getEffectiveLevel() <= logging.DEBUG:
            cv2.imshow('Sheet detection output', display_tools.rescale_image_for_display(img))

    # Use NN to establish if it's a header sheet before trying to remove the header
    if detect_header:
        img = roi_detection.remove_headers(img)
        if logging.getLogger().getEffectiveLevel() <= logging.DEBUG:
            cv2.imshow('Header detection output', display_tools.rescale_image_for_display(img))

    return img


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Code to try and convert pictures of letters into computer text')
    parser.add_argument('--debug', help='Set logging to debug', action='store_true')
    parser.add_argument('--input', help='Input file path', default='data/11_10_20/')
    parser.add_argument('--detectsheet', help='Force the use of sheet detection to apply a mask to the image',
                        action='store_true')
    parser.add_argument('--train', help='Trains the model', action='store_true')
    parser.add_argument('--detectheader',
                        help='Force the use of header detection, to apply a mask to the image to remove them',
                        action='store_true')
    parser.add_argument('--checkmetadata',
                        help='Check metadata describing training data is accurate - specify where the data is stored')
    command_line_arguments = parser.parse_args()

    main(command_line_arguments)
