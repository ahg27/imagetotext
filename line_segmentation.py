import cv2
import logging
import math as maths  # Yes, I'm British, and 'maths' is the hill I'm going to die on
import numpy as np
import scipy
import scipy.stats as stats

import display_tools
import image_preprocessing


def line_segmentation(img):
    lines = line_segmentation_by_finding_guidelines(img)
    if lines is None:
        # Guidelines method didn't work, try another method!
        line_segmentation_by_finding_spaces_between_lines(img)

    return lines


def line_segmentation_by_finding_spaces_between_lines(img):
    """
    A function to take an image and split it into lines, so that these lines can then be split into words
    This tries to accomplish it by looking for the pale parts of the image between each line
    This ignores the helpful guidelines present in some images, but equally this means that it should work in all cases
    :param img: The input image
    :return: A list of line images
    """

    # Invert the image
    # inverted_img = cv2.bitwise_not(img)
    # if logging.getLogger().getEffectiveLevel() <= logging.DEBUG:
    #     cv2.imshow("Inverted image", display_tools.rescale_image_for_display(inverted_img))

    # Make the image binary
    # Not doing this, as there is still too much noise, so applying this ruins the image
    # bw = cv2.adaptiveThreshold(inverted_img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 15, -2)
    # if logging.getLogger().getEffectiveLevel() <= logging.DEBUG:
    #     cv2.imshow("Binary image", display_tools.rescale_image_for_display(bw))

    # Edge detection is not influenced by whether or not the image is inverted
    # canny = image_preprocessing.apply_canny_edge_detection(img, "Edges")
    # if logging.getLogger().getEffectiveLevel() <= logging.DEBUG:
    #     cv2.imshow("Inverted edges", display_tools.rescale_image_for_display(canny))

    # To consider - if applying a mask, then consider if the image is inverted or not
    # The below tries to find lines by establishing where the image is brighter or not (the idea being that where there
    # isn't a bunch of text, the blank paper will change the average  brightness of the line
    # So need to apply a mask in such a was as to not bugger this one up

    """
    width = img.shape[1]
    reduced_img = cv2.reduce(img, 0, cv2.REDUCE_AVG)

    for column in reduced_img:
        for ii, row_brightness in enumerate(column):
            # This isn't working - maybe calculate the tipping point dynamically, so dark areas of the image don't
            # get missed out?  Although how do I draw the line between dark/bright areas of the picture and genuine
            # lines?
            if row_brightness > 175:
                cv2.line(img, (0, ii), (width, ii), color=0, thickness=2)
    """

    return []


def line_segmentation_by_finding_guidelines(img, padding_fraction=0.25):
    """
    A default method to try and break an image into lines, assuming (as is often the case) that my letters were written
    on lined paper.  Use HoughLines to find these lines, and split the image based on that
    :param img: The image of the letter you're looking for
    :param padding_fraction: Assuming that your words might extend below the guideline, how much should padding should
    you add?  Currently assumes symmetry - i.e. the tails of your letters don't drop to overlap with the tops of
    the letters on the next line
    :return: A list of line images
    """

    canny = image_preprocessing.apply_canny_edge_detection(img, "Edges")

    hough_lines = cv2.HoughLinesP(canny, 1, np.pi/180, 400)
    extended_lines = extend_lines_to_image_edge(hough_lines, canny)

    bundler = HoughBundler()
    bundled_lines = bundler.process_horizontal_lines(extended_lines)

    logging.debug(f"Hough Lines found {len(hough_lines)}")
    logging.debug(f"Bundler reduced this to {len(bundled_lines)}")

    if logging.getLogger().getEffectiveLevel() <= logging.DEBUG:
        canny_colour = cv2.cvtColor(canny, cv2.COLOR_GRAY2BGR)  # To draw more visible (colour) lines on
        for line in get_lines(bundled_lines):
            cv2.line(canny_colour, (line[0], line[1]), (line[2], line[3]), (0, 0, 255), 2)
        cv2.imshow('Detected Hough lines', display_tools.rescale_image_for_display(canny_colour))

    # Establish distances between lines, try to find a standard line height after removing outliers
    line_heights = [line[1] for line in get_lines(bundled_lines)]
    line_heights.sort()
    distances_between_lines = [line_heights[i+1] - line_heights[i] for i in range(len(line_heights)-1)]
    z_scores = np.abs(stats.zscore(distances_between_lines))

    # Find a reputable line height, excluding outliers
    # Being quite (very) strict in excluding outliers, to try and exclude inter-line stragglers
    # Must be a better method - I've got inter-line artefacts that can't be *too* far from average, but also lines
    # off the top of the page, which have more flexibility - maybe a Poisson?
    # TODO: Find a better way to remove outliers
    # I will use this number to decide on how much padding to tweak my line segments by
    # Remember that I have found the actual lines you write on - some letters go below that line, and so my line
    # segments need tweaking to take this into account
    # (The amount of tweaking depends on the padding_faction - admittedly a magic constant...
    cleaned_distances = [distance for distance, z_score in zip(distances_between_lines, z_scores) if z_score < 0.5]
    average_line_height = scipy.mean(cleaned_distances)

    # To make the code easier, leave the outliers in and just search for the first time you have a gap between
    # lines that matches the average - hopefully when you first encounter the regular line spacing
    # This will be the gap between the first and second line, so the first line is above this
    first_line_index = next((index for index, z_score in enumerate(z_scores) if abs(z_score) < 0.5), None)

    # TODO: Allow this to take into account missed lines / spurious extra lines
    # This currently just assumes that once I've found a line, I'll find all the subsequent lines.  This is perhaps a
    # bit trusting...
    # (I do discard lines that are too small and are therefore probably not really lines)
    # But I do this in a horribly hacky way
    line_segments = []
    # I don't write on the first line of my letters on the first page, so ignore the first line in this case
    # Assume the first page will have the first line significantly lower down the page then others
    # (Here I define 'significantly lower down' as a couple of lines' height)
    # TODO: Make this more resilient (maybe matching with header detection?)
    if line_heights[first_line_index] > average_line_height*2:
        offset = 1
    else:
        offset = 0

    combined_lines = False
    for ii, line_height in enumerate(line_heights[(first_line_index+offset):]):
        if combined_lines:
            combined_lines = False
            continue
        real_index = ii + first_line_index + offset
        # Check to see if this line is an artefact
        # If it is, missing this line out would give a line closer to average line height than otherwise
        if 0 < real_index < len(line_heights)-1:
            this_line_height = line_height - line_heights[real_index - 1]
            next_line_height = line_heights[real_index + 1] - line_heights[real_index - 1]
            if abs(this_line_height / average_line_height - 1) > abs(next_line_height / average_line_height - 1):
                line_height = line_heights[real_index + 1]
                combined_lines = True
        top_of_segment = int(round(line_height - (average_line_height*(1-padding_fraction))))
        bottom_of_segment = int(round(line_height + (average_line_height*padding_fraction)))
        line_segments.append(img[top_of_segment:bottom_of_segment])

    return line_segments


def extend_lines_to_image_edge(lines, image):
    """
    Extends all images to the limits  of the image, so that we'll examine lines across the entire image, rather
    than several segments of what we would consider to be one line

    PROBLEM - Might lose a pixel's worth of position here and there owing to rounding errors in calculation,
    compounded by numpy casting float to int using floor

    :param lines: The lines as found by HoughLinesP
    :param image: The image you are extending the lines to fill
    :return: The same lines, extended to image edges
    """

    height = image.shape[0]
    width = image.shape[1]

    extended_lines = []

    for line in get_lines(lines):
        x_coords = [line[0], line[2]]
        y_coords = [line[1], line[3]]
        aa = np.vstack([x_coords, np.ones(len(x_coords))]).T
        m, c = np.linalg.lstsq(aa, y_coords, rcond=None)[0]

        left_intercept = c
        right_intercept = m*width + c
        bottom_intercept = (-c) / m
        top_intercept = (height - c) / m

        # This could probably be done using a combination of min() and max()?
        if left_intercept < 0:
            if height < right_intercept:
                extended_line = [bottom_intercept, 0, top_intercept, height]
            else:
                extended_line = [bottom_intercept, 0, width, right_intercept]
        elif height < left_intercept:
            if right_intercept < 0:
                extended_line = [top_intercept, height, bottom_intercept, 0]
            else:
                extended_line = [top_intercept, height, width, right_intercept]
        else:
            if right_intercept < 0:
                extended_line = [0, left_intercept, bottom_intercept, 0]
            elif height < right_intercept:
                extended_line = [0, left_intercept, top_intercept, 0]
            else:
                extended_line = [0, left_intercept, width, right_intercept]

        add_line_to_array(extended_lines, extended_line)

    return extended_lines


def get_lines(lines_in):
    """
    Some reading around suggests that the return format of OpenCV changes after v3.0
    This will protect against that
    N.B. I've not tested this against old versions of OpenCV, and the OpenCV docs don't go back beyond 3.0, so fingers
    crossed, I guess?
    :param lines_in: The lines list as returned by HoughLinesP
    :return: A list of lines in the format [(x1,y1,x2,y2),....]
    """

    if cv2.__version__ < '3.0':
        return lines_in[0]
    for ll in lines_in:
        yield ll[0]


def add_line_to_array(array, line):
    """
    Adds a line defined by 4 points to a list of lines, in a format matching that (hopefully) of HoughLinesP
    :param array: The list of lines we're adding on to
    :param line: A list of 4 numbers we wish to add to the array
    :return:
    """
    array_to_add = np.array(line, dtype=int)
    if cv2.__version__ < '3.0':
        array.append(array_to_add)
    else:
        array.append([array_to_add])


class HoughBundler:
    """
    Cluster and merge each cluster of cv2.HoughLinesP() output
    This code adapted from https://stackoverflow.com/questions/45531074/how-to-merge-lines-after-houghlinesp
    Plenty of scope for improvement, but it works for the moment
    a = HoughBundler()
    foo = a.process_lines(houghP_lines)
    """

    @staticmethod
    def get_orientation(line):
        """
        Get orientation of a line, using its length
        https://en.wikipedia.org/wiki/Atan2

        As it is using the absolute values of the x and y differences, this is limited to 0-90 degrees
        """
        orientation = maths.atan2(abs((line[1] - line[3])), abs((line[0] - line[2])))
        return maths.degrees(orientation)

    def checker(self, line_new, groups, min_distance_to_merge, min_angle_to_merge):
        """
        Check if lines have enough distance and angle to be count as similar
        If similar, the line is added to a group, and the function returns True
        If not similar, the function returns False so that the line can be used to create a new group
        """
        for group in groups:
            # walk through existing line groups
            for line_old in group:
                # check distance
                if self.get_distance(line_old, line_new) < min_distance_to_merge:
                    # check the angle between lines
                    orientation_new = self.get_orientation(line_new)
                    orientation_old = self.get_orientation(line_old)
                    # if all is ok -- line is similar to others in group
                    if abs(orientation_new - orientation_old) < min_angle_to_merge:
                        group.append(line_new)
                        return False
        # if it is totally different line
        return True

    @staticmethod
    def distance_between_point_and_line(point, line):
        """
        Get distance between point and line
        http://local.wasp.uwa.edu.au/~pbourke/geometry/pointline/source.vba
        """
        px, py = point
        x1, y1, x2, y2 = line

        def line_magnitude(x_1, y_1, x_2, y_2):
            """
            Get line (aka vector) length
            """
            return maths.sqrt(maths.pow((x_2 - x_1), 2) + maths.pow((y_2 - y_1), 2))

        magnitude = line_magnitude(x1, y1, x2, y2)
        if magnitude < 0.00000001:
            distance = 9999
            return distance

        u1 = (((px - x1) * (x2 - x1)) + ((py - y1) * (y2 - y1)))
        u = u1 / (magnitude * magnitude)

        if (u < 0.00001) or (u > 1):
            # closest point does not fall within the line segment, take the shorter distance
            # to an endpoint
            ix = line_magnitude(px, py, x1, y1)
            iy = line_magnitude(px, py, x2, y2)
            if ix > iy:
                distance = iy
            else:
                distance = ix
        else:
            # Intersecting point is on the line, use the formula
            ix = x1 + u * (x2 - x1)
            iy = y1 + u * (y2 - y1)
            distance = line_magnitude(px, py, ix, iy)

        return distance

    def get_distance(self, a_line, b_line):
        """
        Get all possible distances between each dot of two lines and second line
        return the shortest
        """
        dist1 = self.distance_between_point_and_line(a_line[:2], b_line)
        dist2 = self.distance_between_point_and_line(a_line[2:], b_line)
        dist3 = self.distance_between_point_and_line(b_line[:2], a_line)
        dist4 = self.distance_between_point_and_line(b_line[2:], a_line)

        return min(dist1, dist2, dist3, dist4)

    def merge_lines_pipeline_2(self, lines):
        """
        Clusters the lines into groups of 'similar' lines
        TODO: Find a more transparent way to merge lines, rather than the magic constants defining similarity
        :param lines: The lines to be grouped
        :return: A list of groups (each group is a list of its constituent lines)
        """
        groups = []  # all lines groups are here
        # Parameters to play with
        min_distance_to_merge = 30
        min_angle_to_merge = 30
        # First line will create a new group every time
        groups.append([lines[0]])
        # If line is different from existing groups, create a new group
        # Otherwise the checker will append the line to an existing group for us
        for line_new in lines[1:]:
            if self.checker(line_new, groups, min_distance_to_merge, min_angle_to_merge):
                groups.append([line_new])

        return groups

    def merge_lines_segments1(self, lines):
        """
        Sort lines cluster and return first and last coordinates
        """
        orientation = self.get_orientation(lines[0])

        # special case
        if len(lines) == 1:
            return [*(lines[0][:2]), *(lines[0][2:])]

        # [[1,2,3,4],[]] to [[1,2],[3,4],[],[]]
        points = []
        for line in lines:
            points.append(line[:2])
            points.append(line[2:])
        # if vertical
        if 45 < orientation < 135:
            # sort by y
            points = sorted(points, key=lambda point: point[1])
        else:
            # sort by x
            points = sorted(points, key=lambda point: point[0])

        # return first and last point in sorted group
        # [x1,y1,x2,y2]
        return [*points[0], *points[-1]]

    def process_lines(self, lines, min_theta=0, max_theta=90):
        """
        Main function for lines from cv.HoughLinesP() output merging
        :param lines: cv.HoughLinesP() output
        :param min_theta: The min angle with the x-axis a line is allowed to make to be considered
        :param max_theta: The max angle with the x-axis a line is allowed to make to be considered
        :return:
        """
        lines_x = []
        lines_y = []
        # for every line of cv2.HoughLinesP()
        for line_i in get_lines(lines):
            orientation = self.get_orientation(line_i)
            if orientation < min_theta or orientation > max_theta:
                continue
            # if vertical
            if 45 < orientation < 90:
                lines_y.append(line_i)
            else:
                lines_x.append(line_i)

        lines_y = sorted(lines_y, key=lambda line: line[1])
        lines_x = sorted(lines_x, key=lambda line: line[0])
        merged_lines_all = []

        # for each cluster in vertical and horizontal lines leave only one line
        for line_list in [lines_x, lines_y]:
            if len(line_list) > 0:
                groups = self.merge_lines_pipeline_2(line_list)
                merged_lines = []
                for group in groups:
                    add_line_to_array(merged_lines, self.merge_lines_segments1(group))
                merged_lines_all.extend(merged_lines)

        return merged_lines_all

    def process_horizontal_lines(self, lines):
        return self.process_lines(lines, max_theta=10)

    def process_vertical_lines(self, lines):
        return self.process_lines(lines, min_theta=80)
